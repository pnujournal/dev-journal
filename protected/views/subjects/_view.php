<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lecture_hours')); ?>:</b>
	<?php echo CHtml::encode($data->lecture_hours); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practic_hours')); ?>:</b>
	<?php echo CHtml::encode($data->practic_hours); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lecturer_id')); ?>:</b>
	<?php echo CHtml::encode($data->lecturer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practic_id')); ?>:</b>
	<?php echo CHtml::encode($data->practic_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
	<?php echo CHtml::encode($data->group_id); ?>
	<br />


</div>