<?php

class LecturersController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        if (Yii::app()->user->getState('role') == "user") {
            $arr = array('');
        } else if (Yii::app()->user->getState('role') == "admin") {
            $arr = array('view', 'create', 'admin', 'delete');
        } else {
            $arr = array('');          //  no access to other user
        }
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => $arr,
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Lecturers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Lecturers'])) {
            $massUser = array();
            $massSU = array();
            $criteria = new CDbCriteria;
            $criteria->addCondition('role=2');
            $criteria->select = 'id';
            $users_id = Users::model()->findAll($criteria);
            foreach ($users_id as $user_id) {
                $massUser[] = $user_id->id;
            }
            $lecturer_users = Lecturers::model()->findAll();
            foreach ($lecturer_users as $lecturer_user) {
                $massSU[] = $lecturer_user->user_id;
            }
            $new_users_id = array_diff($massUser, $massSU); //вибираємо ті які не повторюються
            $new_user_id = array_shift($new_users_id); //вибираємо тільки перший елемент
            $model->attributes = $_POST['Lecturers'];
            $model->user_id = $new_user_id;
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Lecturers('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Lecturers']))
            $model->attributes = $_GET['Lecturers'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Lecturers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'lecturers-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
