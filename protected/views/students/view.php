<?php
$this->menu=array(
	array('label'=>'Добавити студента','url'=>array('create')),
	array('label'=>'Керування студентами','url'=>array('admin')),
);
?>

<h1>View Students #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'surname',
		'birthday',
        array('name' => 'group_id', 'type' => 'text', 'value' => Groups::searchToID($model->group_id)),
		'faculty',
        array('name' => 'stipend', 'type' => 'text', 'value' => $model->stipend == 1 ? 'Має' : 'Не має'),
        array('name' => 'is_hostel', 'type' => 'text', 'value' => $model->is_hostel == 1 ? 'Живе' : 'Не живе'),
		'mobile_number',
		'adress',
		'email',
        'users.login',
        'users.password'
	),
)); ?>
