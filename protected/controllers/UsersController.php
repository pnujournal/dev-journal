<?php

class UsersController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        if (Yii::app()->user->getState('role') == "user") {
            $arr = array('');
        } else if (Yii::app()->user->getState('role') == "admin") {
            $arr = array('create', 'admin');
        } else {
            $arr = array('');          //  no access to other user
        }
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => $arr,
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $mass = array();
        if (isset($_POST['Users'])) {
            $findings = Yii::app()->request->getPost('Users');
            $number = $findings['number'];
            $role = $findings['role'];
            if (!is_numeric($number) || (is_numeric($number) && $number > 10)) {
                $this->redirect($this->createAbsoluteUrl('/site/error', array('access' => 1)));
            }
            for ($i = 0; $i < $number; $i++) {
                $new = $this->generatePasswordAndLogin(10, 7);
                $model = new Users;
                $mass[$i]['role'] = $model->role = $role;
                $mass[$i]['password'] = $model->password = $new['password'];
                $mass[$i]['login'] = $model->login = $new['login'];
                $model->save();
            }
            $this->render('view', array('mass' => $mass));
        }
        $model = new Users;
        $this->render('create', array('model' => $model));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (Yii::app()->user->getState('role') == "admin") {
            $model = new Users('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Users']))
                $model->attributes = $_GET['Users'];

            $this->render('admin', array(
                'model' => $model,
            ));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function generatePasswordAndLogin($numberPassword, $numberLogin)
    {
        $massForPassword = array(
            'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0', '.', ',',
            '(', ')', '[', ']', '!', '?',
            '&', '^', '%', '@', '*', '$',
            '<', '>', '/', '|', '+', '-',
            '{', '}', '`', '~');
        $massForLogin = array(
            'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0',
        );
        // Генерируем пароль
        $password = "";
        for ($i = 0; $i < $numberPassword; $i++) {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($massForPassword) - 1);
            $password .= $massForPassword[$index];
        }
        $login = "";
        for ($i = 0; $i < $numberLogin; $i++) {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($massForLogin) - 1);
            $login .= $massForLogin[$index];
        }
        $result = array('login' => $login, 'password' => $password);
        return $result;
    }

}
