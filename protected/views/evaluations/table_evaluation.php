<h3>Оцінки групи №<?= $group ?> з предмету <?= $subject ?></h3>
<div class='tableholder'>
<?php
$this->widget('bootstrap.widgets.TbGridView',array(
    'type' => 'striped bordered condensed',
//    'template' => "{items}",
	'id'=>'evaluations-grid',
	'dataProvider'=>$evaluations,
	'columns'=>$columns,
));
?>
</div>

