<?php
$this->menu=array(
	array('label'=>'List Evaluations','url'=>array('index')),
	array('label'=>'Create Evaluations','url'=>array('create')),
	array('label'=>'Update Evaluations','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Evaluations','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Evaluations','url'=>array('admin')),
);
?>

<h1>View Evaluations #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'context',
		'points',
		'student_id',
		'subject_id',
		'create_date',
	),
)); ?>
