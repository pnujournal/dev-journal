<center><h1><?= $subject->name ?></h1>
    <h2>оновлення оцінок студенту <?= $student->surname . ' ' . $student->name ?></h2></center>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
    'id' => 'evaluations-form',
    'enableAjaxValidation' => false,
    ));
?>
<?php
//вивід всіх елементів які вже були заповнені
foreach ($evaluations as $evaluation) {
    //видалення зайвих елементів з масиву
    foreach ($contexts as $key => $context) {
        if ($context == $evaluation->context) {
            unset($contexts[$key]);
        }
    }
    echo '<div class="container form-actions span11">';
    echo $form->label($evaluation, 'context', array('label' => 'Назва роботи'));
    echo $form->textField($evaluation, 'context',
        array('class' => 'span5', 'name' => 'Evaluations[context][' . $evaluation->id . ']', 'readonly' => true));
    echo $form->error($evaluation, 'context');
    echo $form->label($evaluation, 'points', array('label' => 'Бали студента'));
    echo $form->textField($evaluation, 'points',
        array('class' => 'span5', 'name' => 'Evaluations[points][' . $evaluation->id . ']'));
    echo $form->error($evaluation, 'points');
    echo '</div>';
}
?>
</div>
<?php
$evaluation = new Evaluations;
foreach ($contexts as $id => $context) {
    echo '<div class="container form-actions span11">';
    echo $form->label($evaluation, 'context', array('label' => 'Назва роботи'));
    echo $form->textField($evaluation, 'context',
        array('class' => 'span5', 'value' => $context, 'name' => 'Evaluations[context][' . $id . ']'));
    echo $form->error($evaluation, 'context');
    echo $form->label($evaluation, 'points', array('label' => 'Бали студента'));
    echo $form->textField($evaluation, 'points[' . $id . ']',
        array('class' => 'span5', 'name' => 'Evaluations[points][' . $id . ']'));
    echo $form->error($evaluation, 'points');
    echo '</div>';
}
echo '<div class="container form-actions span11">';
echo $form->label($evaluation, 'context', array('label' => 'Назва роботи'));
echo $form->textField($evaluation, 'context', array('class' => 'span5', 'name' => 'Evaluations[context][' . 'new' . ']'));
echo $form->error($evaluation, 'context');
echo $form->label($evaluation, 'points', array('label' => 'Бали студента'));
echo $form->textField($evaluation, 'points[' . 'new' . ']',
    array('class' => 'span5', 'name' => 'Evaluations[points][' . 'new' . ']'));
echo $form->error($evaluation, 'points');
echo '</div>';
?>
<div class="container form-actions span11">
    <?php
    $this->widget('bootstrap.widgets.TbButton',
        array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Зберегти',
        'htmlOptions'=>array('class'=>'btn-block', 'style'=>'font-size: 20px'),
    ));
    ?>
</div>
<?php $this->endWidget(); ?>
