<h1>Контакти</h1>

<?php if (Yii::app()->user->hasFlash('contact')): ?>

    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts' => array('contact'),
    ));
    ?>

<?php else: ?>

    <p>
        Якщо ви хочете зв'язатися з адміністраторами, або знийшли баг, будь-ласка заповніть наступну форму для відправки повідомлення. Дякуємо!
    </p>

    <div class="form">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'contact-form',
            'type' => 'horizontal',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>


        <?php echo $form->errorSummary($model); ?>
        
        <?php echo $form->textFieldRow($model, 'name'); ?>

        <?php echo $form->textFieldRow($model, 'email'); ?>

            <?php echo $form->textFieldRow($model, 'subject', array('size' => 60, 'maxlength' => 128)); ?>

            <?php echo $form->textAreaRow($model, 'body', array('rows' => 6, 'class' => 'span8')); ?>

        <div class="form-actions">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => 'Відправити',
            ));
            ?>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- form -->

<?php endif; ?>