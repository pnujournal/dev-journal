<div class='tableholder'>
    <?php
    $this->widget('bootstrap.widgets.TbGridView',
        array(
        'dataProvider' => $dataProvider,
        'type' => 'striped bordered condensed',
        'template' => "{items}",
        'columns' => array(
            array(
                'name' => 'name',
                'header' => 'Ім’я групи',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, Yii::app()->createUrl("/evaluations/allsubjectgroup", array("id"=>$data->id)))',
            ),
            'short_name',
            'count_students',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{view}{delete}',
                'buttons' => array(
                    'delete' => array(
                        'visible' => 'Yii::app()->user->getState(\'role\') == "admin"',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div>
