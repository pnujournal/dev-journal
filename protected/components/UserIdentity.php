<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $users = array(
            'admin' => array('password'=>'admin', 'role' => 'admin')
        );
        $usersDb = Users::model()->findAll();
        foreach ($usersDb as $user) {
            $users[$user->login] = array('password' => $user->password, 'role' => $user->role == 2 ? 'admin' : 'user');
        }
        if (!isset($users[$this->username])) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($users[$this->username]['password'] !== $this->password) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            $this->setState('role', $users[$this->username]['role']);
        }
        return !$this->errorCode;
    }

}
