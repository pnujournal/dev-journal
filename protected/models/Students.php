<?php

/**
 * This is the model class for table "Students".
 *
 * The followings are the available columns in table 'Students':
 * @property integer $id
 * @property integer $users_id
 * @property string $name
 * @property string $surname
 * @property string $birthday
 * @property integer $group_id
 * @property string $faculty
 * @property integer $stipend
 * @property integer $is_hostel
 * @property string $mobile_number
 * @property string $adress
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Evaluations[] $evaluations
 * @property Users $users
 * @property Groups $group
 */
class Students extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Students';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('users_id, name, surname, group_id', 'required'),
			array('users_id, group_id, stipend, is_hostel', 'numerical', 'integerOnly'=>true),
			array('name, surname, faculty, adress, email', 'length', 'max'=>255),
			array('mobile_number', 'length', 'max'=>11),
			array('birthday', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, users_id, name, surname, birthday, group_id, faculty, stipend, is_hostel, mobile_number, adress, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'evaluations' => array(self::HAS_MANY, 'Evaluations', 'student_id'),
			'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
			'group' => array(self::BELONGS_TO, 'Groups', 'group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'users_id' => 'ID користувача',
			'name' => 'Ім’я студента',
			'surname' => 'Прізвище',
			'birthday' => 'День народження',
			'group_id' => 'Група',
			'faculty' => 'Факультет',
			'stipend' => 'Стипендія',
			'is_hostel' => 'Гуртожиток',
			'mobile_number' => 'Мобільний номер',
			'adress' => 'Адреса',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('users_id',$this->users_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('faculty',$this->faculty,true);
		$criteria->compare('stipend',$this->stipend);
		$criteria->compare('is_hostel',$this->is_hostel);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('adress',$this->adress,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Students the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
