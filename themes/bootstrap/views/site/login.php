
<h1 class="span12">Вхід</h1>
<div class="form ">
<div class="span10 center_t">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
        'id' => 'login-form',
        'type' => 'vertical',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>
  </div>
    <div class="span1"></div>
    <div class="form span12">
        <?php echo $form->label($model, 'username',array('label' => 'Логін'));?>
        <?php echo $form->textFieldRow($model, 'username', array('labelOptions' => array('label' => false))); ?>
        <?php echo $form->label($model, 'username',array('label' => 'Пароль'));?>
        <?php echo $form->passwordFieldRow($model, 'password', array('labelOptions' => array('label' => false))); ?>
        <?php echo '<br>'?>
  
        <?php
        $this->widget('bootstrap.widgets.TbButton',
            array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'htmlOptions'=>array('style'=>'width:220px'),
            'label'=>'Увійти',
        ));
        ?>
    </div>
    

<?php $this->endWidget(); ?>
</div>
