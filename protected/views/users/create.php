<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

<h1>Генерація користувачів</h1>
<?php
echo $form->label($model, 'login', array('label' => 'Введіть скільки згенерувати логінів та паролей<br><small>Кількість генерацій повинна бути числом і не перевищувати 10 записів</small>'));
echo $form->textField($model, 'login', array('class' => 'span5', 'name' => 'Users[number]'));
echo $form->error($model, 'login');
echo $form->label($model, 'role', array('label' => 'Для якої ролі?(1-студентів,2-викладачів)'));
echo $form->textField($model, 'role', array('class' => 'span5', 'name' => 'Users[role]'));
echo $form->error($model, 'role');
?>
<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton',
        array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Додати',
    ));
    ?>
</div>
<?php $this->endWidget(); ?>