<?php
$this->menu = array(
    array('label' => 'Добавити студента', 'url' => array('create')),
);

?>

<h1>Керування студентами</h1>

<p>
    Можете для пошуку використовувати оператори (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    або <b>=</b>) після введення інформації для пошуку натисніть Enter.
</p>


<?php
$this->widget('bootstrap.widgets.TbGridView',
    array(
    'id' => 'students-grid',
    'type' => 'striped bordered condensed',
    'template' => "{items}",
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'name',
        'surname',
        'users.login',
        'users.password',
        /*
          'faculty',
          'stipend',
          'is_hostel',
          'mobile_number',
          'adress',
          'email',
         */
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'updateButtonUrl' => null,
        ),
    ),
));
?>
