<?php

/**
 * This is the model class for table "Subjects".
 *
 * The followings are the available columns in table 'Subjects':
 * @property integer $id
 * @property string $name
 * @property integer $lecture_hours
 * @property integer $practic_hours
 * @property integer $lecturer_id
 * @property integer $practic_id
 * @property integer $group_id
 *
 * The followings are the available model relations:
 * @property Evaluations[] $evaluations
 * @property Groups $group
 * @property Lecturers $lecturer
 * @property Lecturers $practic
 */
class Subjects extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Subjects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('lecture_hours, practic_hours, lecturer_id, practic_id, group_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, lecture_hours, practic_hours, lecturer_id, practic_id, group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'evaluations' => array(self::HAS_MANY, 'Evaluations', 'subject_id'),
			'group' => array(self::BELONGS_TO, 'Groups', 'group_id'),
			'lecturer' => array(self::BELONGS_TO, 'Lecturers', 'lecturer_id'),
			'practic' => array(self::BELONGS_TO, 'Lecturers', 'practic_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Iм\'я',
			'lecture_hours' => 'Лекційні години',
			'practic_hours' => 'Практичні години',
			'lecturer_id' => 'Викладач лекцій',
			'practic_id' => 'Викладач практики',
			'group_id' => 'Група',
            'fullLectorName' => 'Викладач лекцій',
            'fullPracticName' => 'Викладач практики',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lecture_hours',$this->lecture_hours);
		$criteria->compare('practic_hours',$this->practic_hours);
		$criteria->compare('lecturer_id',$this->lecturer_id);
		$criteria->compare('practic_id',$this->practic_id);
		$criteria->compare('group_id',$this->group_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subjects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getFullLectorName()
    {
        return $this->lecturer->name.' '.$this->lecturer->surname;
    }

    public function getFullPracticName()
    {
        return $this->practic->name.' '.$this->practic->surname;
    }
}
