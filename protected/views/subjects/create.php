<?php
$this->menu = array(
    array('label' => 'List Subjects', 'url' => array('index')),
    array('label' => 'Manage Subjects', 'url' => array('admin')),
);
?>

<h1>Додати предмет</h1>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
    'id' => 'subjects-form',
    'enableAjaxValidation' => false,
    ));
?>

<p class="help-block">Поля із зірочкою (<span class="required">*</span>) є обов'язковими.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'lecture_hours', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'practic_hours', array('class' => 'span5')); ?>

<?php echo $form->label($model, 'lecturer_id', array('label' => 'Виберіть викладача який читає лекції : ')); ?>
<?php echo $form->dropDownList($model, 'lecturer_id', $lecturer_id, array('class' => 'span5')); ?>

<?php echo $form->label($model, 'practic_id', array('label' => 'Виберіть викладача який проводить практику : ')); ?>
<?php echo $form->dropDownList($model, 'practic_id', $lecturer_id, array('class' => 'span5')); ?>

<?php echo $form->label($model, 'group_id', array('label' => 'Виберіть групу : ')); ?>
<?php echo $form->dropDownList($model, 'group_id', $group_id, array('class' => 'span5')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton',
        array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Додати',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>