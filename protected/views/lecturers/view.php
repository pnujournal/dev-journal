<?php
$this->menu=array(
	array('label'=>'Добавити викладача','url'=>array('create')),
	array('label'=>'Керування списком викладачів','url'=>array('admin')),
);
?>

<h1>View Lecturers #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'name',
		'surname',
		'faculty',
		'department',
		'birthday',
		'mobile_number',
		'email',
        'users.login',
        'users.password'
	),
)); ?>
