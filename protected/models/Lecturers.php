<?php

/**
 * This is the model class for table "Lecturers".
 *
 * The followings are the available columns in table 'Lecturers':
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $surname
 * @property string $faculty
 * @property string $department
 * @property string $birthday
 * @property string $mobile_number
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Groups[] $groups
 * @property Users $user
 * @property Subjects[] $subjects
 * @property Subjects[] $subjects1
 */
class Lecturers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Lecturers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, name, surname, faculty, department', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('name, surname, faculty, department, mobile_number, email', 'length', 'max'=>255),
			array('birthday', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, name, surname, faculty, department, birthday, mobile_number, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'groups' => array(self::HAS_MANY, 'Groups', 'curator_id'),
			'users' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'subjects' => array(self::HAS_MANY, 'Subjects', 'lecturer_id'),
			'subjects1' => array(self::HAS_MANY, 'Subjects', 'practic_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'ID Користувача',
			'name' => 'Ім’я',
			'surname' => 'Прізвище',
			'faculty' => 'Факультет',
			'department' => 'Кафедра',
			'birthday' => 'День народження',
			'mobile_number' => 'Мобільний номер',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('faculty',$this->faculty,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lecturers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
