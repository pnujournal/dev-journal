<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::encode($data->id); ?>
	<br />

	<b><?php echo "Повне ім'я групи "; ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->name), Yii::app()->baseUrl . '/index.php/evaluations/allsubjectgroup?id='.$data->id); ?>
	<br />

	<b><?php echo "Скорочене ім'я групи "; ?>:</b>
	<?php echo CHtml::encode($data->short_name); ?>
	<br />

	<b><?php echo "Кількість студентів "; ?>:</b>
	<?php echo CHtml::encode($data->count_students); ?>
	<br />

	<b><?php echo "Куратор "; ?>:</b>
	<?php echo CHtml::encode($data->curator_id); ?>
	<br />


</div>