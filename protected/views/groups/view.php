<?php
$this->menu=array(
	array('label'=>'Створити групу','url'=>array('create')),
	array('label'=>'Керування групами','url'=>array('admin')),
);
?>

<h1>View Groups #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'short_name',
		'count_students',
        array(
            'name'=>'curator.surname',
            'label'=>'Прізвище куратора',
        ),
        array(
            'name'=>'curator.name',
            'label'=>'Ім’я куратора',
        ),
	),
)); ?>
