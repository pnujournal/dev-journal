<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subjects-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля із зірочкою (<span class="required">*</span>) є обов'язковими.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'lecture_hours',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'practic_hours',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lecturer_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'practic_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'group_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Додати' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
