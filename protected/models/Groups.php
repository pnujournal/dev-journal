<?php

/**
 * This is the model class for table "Groups".
 *
 * The followings are the available columns in table 'Groups':
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property integer $count_students
 * @property integer $curator_id
 *
 * The followings are the available model relations:
 * @property Lecturers $curator
 * @property Students[] $students
 * @property Subjects[] $subjects
 */
class Groups extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Groups';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, short_name, count_students', 'required'),
			array('count_students, curator_id', 'numerical', 'integerOnly'=>true),
			array('name, short_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, short_name, count_students, curator_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'curator' => array(self::BELONGS_TO, 'Lecturers', 'curator_id'),
			'students' => array(self::HAS_MANY, 'Students', 'group_id'),
			'subjects' => array(self::HAS_MANY, 'Subjects', 'group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Повне ім’я групи',
			'short_name' => 'Скорочене ім’я групи',
			'count_students' => 'Кількість студентів у групі',
			'curator_id' => 'Куратор',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short_name',$this->short_name,true);
		$criteria->compare('count_students',$this->count_students);
		$criteria->compare('curator_id',$this->curator_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function searchToID($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->addCondition('id='.$id);
        $group = self::model()->find($criteria);
		return $group->name;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Groups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
