<?php
$this->menu=array(
	array('label'=>'Створити групу','url'=>array('create')),
);
?>

<h1>Manage Groups</h1>

<p>
Можете для пошуку використовувати оператори (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    або <b>=</b>) після введення інформації для пошуку натисніть Enter.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'groups-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'short_name',
		'count_students',
		'curator_id',
		array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'updateButtonUrl' => null,
        ),
	),
)); ?>
