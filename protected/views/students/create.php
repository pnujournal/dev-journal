<?php
$this->menu = array(
    array('label' => 'Керування студентами', 'url' => array('admin')),
);
?>

<h1>Додати студента</h1>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
    'id' => 'students-form',
    'enableAjaxValidation' => false,
    ));
?>

<p class="help-block">Поля із зірочкою (<span class="required">*</span>) є обов'язковими.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'surname', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'birthday', array('class' => 'span5','placeholder'=>'Формат введення рррр-мм-дд. (Наприклад 2016-06-20)')); ?>

<?php echo $form->label($model, 'group_id', array('label' => 'Виберіть групу студента : '));?>
<?php echo $form->dropDownList($model,'group_id', $group_id, array('class'=>'span5'));?>

<?php echo $form->textFieldRow($model, 'faculty', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->label($model, 'stipend', array('label' => 'Має стипендію?'), array('class' => 'span5'));?>
<?php echo $form->checkBox($model,'stipend',  array('checked'=>'checked'), array('class' => 'span5')); ?>

<?php echo $form->label($model, 'is_hostel', array('label' => 'Живе в гуртожитку?'), array('class' => 'span5'));?>
<?php echo $form->checkBox($model,'is_hostel',  array('checked'=>'checked'), array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'mobile_number', array('class' => 'span5', 'maxlength' => 11)); ?>

<?php echo $form->textFieldRow($model, 'adress', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton',
        array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Додати' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>