<?php
$this->menu=array(
	array('label'=>'Керування групами','url'=>array('admin')),
);
?>

<h1>Додати групу</h1>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'groups-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля із зірочкою (<span class="required">*</span>) є обов'язковими.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'short_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'count_students',array('class'=>'span5')); ?>

    <?php echo $form->label($model, 'curator_id', array('label' => 'Виберіть куратора групи : '));?>
    <?php echo $form->dropDownList($model,'curator_id', $curator_id, array('class'=>'span5'));?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Додати' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>