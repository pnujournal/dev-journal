<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="ru"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="ru"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="ru"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Сраница ошибки 404</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="/css/error.css">
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    </head>
    <body>
        <div class="comingcontainer">
            <div class="checkbacksoon">
                <p>
                    <span class="go3d">4</span>
                    <span class="go3d">0</span>
                    <span class="go3d">4</span>
                    <span class="go3d">!</span>

                </p>

                <p class="error">
                    Ви ввели або вибрали неправильний шлях.<br> Не хвилюйтесь, час від часу, це трапляється з кожним із нас.</p>
                <?php if (Yii::app()->user->getState('role')): ?>
                    <p class="error"> Ви можете написати адміністратору на сторінці <b>КОНТАКТИ</b> нижче.</p>
                    <nav>
                        <?php
                        $this->widget('bootstrap.widgets.TbNavbar',
                            array(
                            'collapse' => true,
                            'items' => array(
                                array(
                                    'class' => 'bootstrap.widgets.TbMenu',
                                    'htmlOptions' => array('class' => 'pull-right'),
                                    'type' => 'navbar',
                                    'items' => array(
                                        array('label' => 'Головна', 'url' => array('/site/index')),
                                        array('label' => 'Всі групи', 'url' => array('/groups/')),
                                        array('label' => 'Контакти', 'url' => array('/site/contact')),
                                    ),
                                ),
                            ),
                        ));
                        ?>
                    </nav>
                <?php else: ?>
                    <p class="error">Вам потрібно авторизуватись для роботи з сайтом за посиланням <a href=<?= Yii::app()->createAbsoluteUrl('/') ?>><b>ВХІД</b></a></p>
                    <?php endif; ?>
            </div>
        </div>

    </body>
</html>