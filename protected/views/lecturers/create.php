<?php
$this->menu=array(
	array('label'=>'Керування списком викладачів','url'=>array('admin')),
);
?>

<h1>Додати викладача</h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'lecturers-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля із зірочкою (<span class="required">*</span>) є обов'язковими.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'surname',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'faculty',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'department',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model, 'birthday', array('class' => 'span5','placeholder'=>'Формат введення рррр-мм-дд. (Наприклад 2016-06-20)')); ?>

	<?php echo $form->textFieldRow($model,'mobile_number',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Додати' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
