<h1>Предмети групи #<?php echo $group->name; ?></h1>

<?php
foreach ($subjects as $value) {
    $this->widget('bootstrap.widgets.TbDetailView',
        array(
        'data' => $value,
        'attributes' => array(
            array(
                'label' => 'Назва предмету',
                'type' => 'raw',
                'value' => CHtml::link(
                    CHtml::encode($value->name), Yii::app()->baseUrl . '/index.php/evaluations/tableforsubject?group_id='.$value->group_id.'&subject_id='.$value->id
                ),
            ),
            array('name' => 'Лекційні години', 'value' => $value->lecture_hours),
            array('name' => 'Практичні години', 'value' => $value->practic_hours),
        ),
    ));
}
?>
