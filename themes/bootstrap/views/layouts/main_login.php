<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <?php Yii::app()->bootstrap->register(); ?>
    </head>

    <body class="login_bg">

        <div class="container" id="page">
            <?php echo $content; ?>
            <div class="clear"></div>
            </div>
            <div id="footer" class="footer_my">
                <div class="container1">
                    <p class="text-muted text-center">Прикарпатський Національний Університет ім. Василя Стефаника</p>
                </div>
            </div><!-- footer -->

        <!-- page -->

    </body>
</html>
