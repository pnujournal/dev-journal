<?php
$this->menu=array(
	array('label'=>'Добавити викладача','url'=>array('create')),
);
?>

<h1>Manage Lecturers</h1>

<p>
Можете для пошуку використовувати оператори (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    або <b>=</b>) після введення інформації для пошуку натисніть Enter.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'lecturers-grid',
    'type' => 'striped bordered condensed',
    'template' => "{items}",
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
//		'user_id',
		'name',
		'surname',
		'faculty',
		'department',
        'users.login',
        'users.password',
		/*
		'birthday',
		'mobile_number',
		'email',
		*/
		array(
			'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'updateButtonUrl' => null,
		),
	),
)); ?>
