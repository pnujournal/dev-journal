<?php

class EvaluationsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        if (Yii::app()->user->getState('role') == "user") {
            $arr = array('allsubjectgroup', 'tableforsubject');
        } else if (Yii::app()->user->getState('role') == "admin") {
            $arr = array('index', 'view', 'allsubjectgroup', 'tableforsubject', 'create', 'update', 'admin', 'delete', 'updateStudent');
        } else {
            $arr = array('');          //  no access to other user
        }
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => $arr,
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Evaluations;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Evaluations'])) {
            $model->attributes = $_POST['Evaluations'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Evaluations'])) {
            $model->attributes = $_POST['Evaluations'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Evaluations');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Evaluations('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Evaluations']))
            $model->attributes = $_GET['Evaluations'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionAllSubjectGroup($id)
    {
        if (isset($id)) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('group_id=' . $id);
            $subjects = Subjects::model()->findAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->addCondition('id=' . $id);
            $group = Groups::model()->find($criteria);
            $this->render('table',
                array(
                'subjects' => $subjects,
                'group' => $group,
            ));
        }
    }

    public function actionTableForSubject($group_id, $subject_id, $id = FALSE)
    {
        $this->layout = 'front';
        if (!empty($group_id)) {
            $columns = $evaluations = $contexts = array();
            //search - students
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.group_id=' . $group_id);
            $criteria->order = 't.surname, evaluations.context';
            $criteria->with = 'evaluations';
//            $criteria->addCondition('evaluations.subject_id=' . $subject_id);
            $students = Students::model()->findAll($criteria);
            //search - subject
            $criteria = new CDbCriteria;
            $criteria->addCondition('subject_id=' . $subject_id);
            $criteria->order = 'context';
            $criteria->group = 'context';
            $criteria->select = array('context');
            $subjects = Evaluations::model()->findAll($criteria);
            $columns[] = array('name' => 'Студенти | Тип роботи >>', 'value' => '$data["student"]');
            foreach ($subjects as $subject) {
                $columns[] = array('name' => $subject->context, 'value' => '$data["' . $subject->context . '"]');
                $contexts[] = $subject->context;
            }
            if (Yii::app()->user->getState('role') == "admin") {
                $columns[] = array(
                    'class' => 'CButtonColumn',
                    'template' => '{update}',
                    'buttons' => array(
                        'update' => array(
                            'url' => 'Yii::app()->createUrl("/evaluations/updateStudent", array("id"=>$data["id"], "subject_id" => ' . $subject_id . '))',
                        ),
                    ),
                );
            }
            foreach ($students as $student) {
                $evaluations[$student->id]['student'] = $student->surname . ' ' . $student->name;
                $evaluations[$student->id]['id'] = $student->id;
                foreach ($student->evaluations as $evaluation) {
                    if (in_array($evaluation->context, $contexts)) {
                        $evaluations[$student->id][$evaluation->context] = $evaluation->points;
                    }
                }
            }
            $evaluations = new CArrayDataProvider($evaluations, array('id' => '$evaluations', 'pagination' => array('pageSize' => 25),));
            $criteria = new CDbCriteria;
            $criteria->addCondition('id=' . $subject_id);
            $subject = Subjects::model()->find($criteria);
            $criteria = new CDbCriteria;
            $criteria->addCondition('id=' . $group_id);
            $group = Groups::model()->find($criteria);

            $this->render('table_evaluation',
                array(
                'columns' => $columns,
                'evaluations' => $evaluations,
                'subject' => $subject->name,
                'group' => $group->short_name,
            ));
        }
    }

    public function actionUpdateStudent($id, $subject_id)
    {
        $evaluations = Evaluations::model()->findAllByAttributes(array('student_id' => $id, 'subject_id' => $subject_id));
        $student = Students::model()->findByPk($id);
        $criteria = new CDbCriteria;
        $criteria->addCondition('subject_id=' . $subject_id);
        $criteria->order = 'context';
        $criteria->group = 'context';
        $subjects = Evaluations::model()->findAll($criteria);
        $contexts = array();
        foreach ($subjects as $subject) {
            $contexts[$subject->id] = $subject->context;
        }
        //Збереження в БД
        if (isset($_POST['Evaluations'])) {
            $context_point = Yii::app()->request->getPost('Evaluations');
            $context = $context_point['context'];
            $point = $context_point['points'];
            foreach ($context as $key => $value) {
                $new = Evaluations::model()->findByAttributes(array('student_id' => $id, 'subject_id' => $subject_id, 'context' => $value));
                if (!empty($new)) {
                    $model = $this->loadModel($key);
                    if($point[$key] != 0){
                    $model->points = $point[$key];
                    $model->create_date = date("Y-m-d");
                    $model->save();
                    }else{
                        $model->delete();
                    }
                } else {
                    if (!empty($point[$key])) {
                        $model = new Evaluations;
                        $model->context = $value;
                        $model->points = $point[$key];
                        $model->student_id = $id;
                        $model->subject_id = $subject_id;
                        $model->create_date = date("Y-m-d");
                        $model->save();
                    }
                }
            }
            $group_id = Students::model()->findByAttributes(array('id' => $id));
            $group_id = $group_id->group_id;
            $this->redirect(array('evaluations/tableforsubject', 'group_id' => $group_id, 'subject_id' => $subject_id));
        }
        $subject = Subjects::model()->findByPk($subject_id);
        $this->render('updateStudent',
            array(
            'evaluations' => $evaluations,
            'student' => $student,
            'subject' => $subject,
            'contexts' => $contexts,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Evaluations::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'evaluations-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
