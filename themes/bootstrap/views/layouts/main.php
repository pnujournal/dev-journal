<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <?php Yii::app()->bootstrap->register(); ?>
    </head>

    <body>

        <?php
        $this->widget('bootstrap.widgets.TbNavbar',
            array(
            'collapse'=>true,
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'htmlOptions'=>array('class'=>'pull-right'),
                    'type' => 'navbar',
                    'items' => array(
                        array('label' => 'Головна', 'url' => array('/site/index')),
                        array('label' => 'Всі групи', 'url' => array('/groups/')),
                        array(
                            'label' => 'Додати',
                            'items' => Yii::app()->user->getState('role') != "admin" ? array() : array(
                                array('label' => 'Користувача', 'url' => array('/users/create')),
                                array('label' => 'Студента', 'url' => array('/students/create')),
                                array('label' => 'Викладача', 'url' => array('/lecturers/create')),
                                array('label' => 'Групу', 'url' => array('/groups/create')),
                                array('label' => 'Предмет', 'url' => array('/subjects/create')),
                                ),
                        ),
                        array('label' => 'Контакти', 'url' => array('/site/contact')),
                        array('label' => 'Вихід (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                    ),
                ),
            ),
        ));
        ?>

        <div class="container" id="page">

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs',
                    array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
                <?php endif ?>
            <div class='viewholder'>
<?php echo $content; ?>
            </div>
            <div class="clear"></div>


        </div><!-- page -->

    </body>
</html>
